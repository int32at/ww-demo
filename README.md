### CI/CD Pipeline Demo
This is a simple demo how a CI/CD pipeline could work based on the discussion we had 29th April 2020. Obviously lots of things could be improved but I hope it gets the point across!

## App
In this case the application that will be deployed as docker image is very simple and only contains the `api.php` which will echo the environment variable `ENVIRONMENT`. This is to simulate the many different environment configurations that the actual Laravel app will be adapted to.

## Tests
This demo does not include any unit/integration tests for the app itself. In the actual project these will be added as multi-stage builds in the build process of the docker image.

The test that exist (`test.sh`) will actually check the running docker container that will be spawned by the `test-*` stages of the CI process.

## CI / CD Pipeline

The pipeline consists of 3 parts:

- **build**: will build the actual docker image based on the source code of the app and upload it to the registry tagged with the commit's hash.

- **test-***: will pull the image and run it based on it's environment config. The script will wait until the container and php server started (not ideal yet but works) and then query and check for the correct environment config.

- **deploy**: will pull the image and re-tag is as latest and push it to the registry.
