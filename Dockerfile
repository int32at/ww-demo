FROM php:7.4-cli
WORKDIR /app
COPY . /app
EXPOSE 3000
CMD [ "php", "-S", "0.0.0.0:3000", "/app/api.php" ]
