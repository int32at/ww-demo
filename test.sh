echo "running test"
expected=$1
actual=$(curl docker:3000)
echo "expecting $expected, got $actual"
[ "$actual" == "$expected" ] && echo "test success" && exit 0 || echo "test failed" && exit -1
